let object = {
  name: 'Aidar',
  age: 30
}

object = new Proxy(object, {
  get(target, prop) {
    if(prop === 'name') {
      return 'closed!'
    } else {
      return target[prop];
    }
  },
  set(target, prop, val) {
    if(prop === 'age') {
      target[prop] = Math.max(val, 0);
    } else {
      target[prop] = val;
    }
    return true;
  }
})

object.age = -1;

console.log(object.name, object.age);